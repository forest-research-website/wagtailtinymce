/*
Copyright (c) 2016, Isotoma Limited
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Isotoma Limited nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL ISOTOMA LIMITED BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

TINYMCE_IMAGE_CHOOSER_MODAL_ONLOAD_HANDLERS = {
  chooser: function(modal, jsonData) {
    var searchUrl = $('form.image-search', modal.body).attr('action');

    /* currentTag stores the tag currently being filtered on, so that we can
        preserve this when paginating */
    var currentTag;

    function ajaxifyLinks(context) {
      $('.listing a', context).on('click', function() {
        modal.loadUrl(this.href);
        return false;
      });

      $('.pagination a', context).on('click', function() {
        var page = this.getAttribute('data-page');
        setPage(page);
        return false;
      });
    }

    function fetchResults(requestData) {
      $.ajax({
        url: searchUrl,
        data: requestData,
        success: function(data, status) {
          $('#image-results').html(data);
          ajaxifyLinks($('#image-results'));
        }
      });
    }

    function search() {
      /* Searching causes currentTag to be cleared - otherwise there's
            no way to de-select a tag */
      currentTag = null;
      fetchResults({
        q: $('#id_q').val(),
        collection_id: $('#collection_chooser_collection_id').val()
      });
      return false;
    }

    function setPage(page) {
      params = { p: page };
      if ($('#id_q').val().length) {
        params['q'] = $('#id_q').val();
      }
      if (currentTag) {
        params['tag'] = currentTag;
      }
      params['collection_id'] = $('#collection_chooser_collection_id').val();
      fetchResults(params);
      return false;
    }

    ajaxifyLinks(modal.body);

    $('form.image-upload', modal.body).on('submit', function() {
      var formdata = new FormData(this);

      if ($('#id_image-chooser-upload-title', modal.body).val() == '') {
        var li = $('#id_image-chooser-upload-title', modal.body).closest('li');
        if (!li.hasClass('error')) {
          li.addClass('error');
          $('#id_image-chooser-upload-title', modal.body)
            .closest('.field-content')
            .append(
              '<p class="error-message"><span>This field is required.</span></p>'
            );
        }
        setTimeout(cancelSpinner, 500);
      } else {
        $.ajax({
          url: this.action,
          data: formdata,
          processData: false,
          contentType: false,
          type: 'POST',
          dataType: 'text',
          success: modal.loadResponseText,
          error: function(response, textStatus, errorThrown) {
            message =
              jsonData['error_message'] +
              '<br />' +
              errorThrown +
              ' - ' +
              response.status;
            $('#upload').append(
              '<div class="help-block help-critical">' +
                '<strong>' +
                jsonData['error_label'] +
                ': </strong>' +
                message +
                '</div>'
            );
          }
        });
      }

      return false;
    });

    $('form.image-search', modal.body).on('submit', search);

    $('#id_q').on('input', function() {
      clearTimeout($.data(this, 'timer'));
      var wait = setTimeout(search, 200);
      $(this).data('timer', wait);
    });
    $('#collection_chooser_collection_id').on('change', search);
    $('a.suggested-tag').on('click', function() {
      currentTag = $(this).text();
      $('#id_q').val('');
      fetchResults({
        tag: currentTag,
        collection_id: $('#collection_chooser_collection_id').val()
      });
      return false;
    });

    function populateTitle(context) {
      var fileWidget = $('#id_image-chooser-upload-file', context);
      fileWidget.on('change', function() {
        var titleWidget = $('#id_image-chooser-upload-title', context);
        var title = titleWidget.val();
        if (title === '') {
          // The file widget value example: `C:\fakepath\image.jpg`
          var parts = fileWidget.val().split('\\');
          var fileName = parts[parts.length - 1];
          titleWidget.val(fileName);
        }
      });
    }

    populateTitle(modal.body);
  },
  image_chosen: function(modal, jsonData) {
    modal.respond('imageChosen', jsonData['result']);
    modal.close();
  },
  select_format: function(modal) {
    $('form', modal.body).on('submit', function() {
      var formdata = new FormData(this);

      $.post(this.action, $(this).serialize(), modal.loadResponseText, 'text');

      return false;
    });
  }
};

(function() {
  'use strict';

  (function($) {
    tinymce.PluginManager.add('wagtailimage', function(editor) {
      /* stop editing and resizing of embedded image content */
      function fixContent() {
        $(editor.getBody())
          .find('[data-embedtype=image]')
          .each(function() {
            $(this)
              .attr('contenteditable', false)
              .attr('data-mce-contenteditable', 'false')
              .find('div,table,img')
              .attr('data-mce-resize', 'false');
          });
      }

      function showDialog() {
        var url,
          urlParams,
          mceSelection,
          $currentNode,
          $targetNode,
          insertElement;

        mceSelection = editor.selection;
        $currentNode = $(mceSelection.getEnd());
        // target selected image (if any)
        $targetNode = $currentNode.closest('[data-embedtype=image]');
        if ($targetNode.length) {
          url = window.chooserUrls.imageChooserSelectFormat;
          url = url.replace('00000000', $targetNode.data('id'));
          urlParams = {
            edit: 1,
            format: $targetNode.data('format'),
            alt_text: $targetNode.data('alt'),
            caption: $targetNode.data('caption')
          };
          // select and replace target
          insertElement = function(elem) {
            mceSelection.select($targetNode.get(0));
            mceSelection.setNode(elem);
          };
        } else {
          url = window.chooserUrls.imageChooser;
          urlParams = {
            select_format: true
          };
          // otherwise target immediate child of nearest div container
          $targetNode = $currentNode
            .parentsUntil('div:not([data-embedtype])')
            .not('body,html')
            .last();
          if (0 == $targetNode.length) {
            // otherwise target current node
            $targetNode = $currentNode;
          }
          // select and insert after target
          insertElement = function(elem) {
            $(elem).insertBefore($targetNode);
            mceSelection.select(elem);
          };
        }

        ModalWorkflow({
          url: url,
          urlParams: urlParams,
          onload: TINYMCE_IMAGE_CHOOSER_MODAL_ONLOAD_HANDLERS,
          responses: {
            imageChosen: function(imageData) {
              var elem = $(imageData.html).get(0);
              editor.undoManager.transact(function() {
                editor.focus();
                insertElement(elem);
                fixContent();
              });
            }
          }
        });
      }

      editor.addButton('wagtailimage', {
        icon: 'image',
        tooltip: 'Insert/edit image',
        onclick: showDialog,
        stateSelector: '[data-embedtype=image]'
      });

      editor.addMenuItem('wagtailimage', {
        icon: 'image',
        text: 'Insert/edit image',
        onclick: showDialog,
        context: 'insert',
        prependToContext: true
      });

      editor.addCommand('mceWagtailImage', showDialog);

      editor.on('LoadContent', function(e) {
        fixContent();
      });
    });
  })(jQuery);
}.call(this));
