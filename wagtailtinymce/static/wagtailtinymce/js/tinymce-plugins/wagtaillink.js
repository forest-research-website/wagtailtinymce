/*
Copyright (c) 2016, Isotoma Limited
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Isotoma Limited nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL ISOTOMA LIMITED BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

TINYMCE_PAGE_CHOOSER_MODAL_ONLOAD_HANDLERS = {
  browse: function(modal, jsonData) {
    /* Set up link-types links to open in the modal */
    $('.link-types a', modal.body).on('click', function() {
      modal.loadUrl(this.href);
      return false;
    });

    /*
      Set up submissions of the search form to open in the modal.

      FIXME: wagtailadmin.views.chooser.browse doesn't actually return a modal-workflow
      response for search queries, so this just fails with a JS error.
      Luckily, the search-as-you-type logic below means that we never actually need to
      submit the form to get search results, so this has the desired effect of preventing
      plain vanilla form submissions from completing (which would clobber the entire
      calling page, not just the modal). It would be nice to do that without throwing
      a JS error, that's all...
      */
    modal.ajaxifyForm($('form.search-form', modal.body));

    /* Set up search-as-you-type behaviour on the search box */
    var searchUrl = $('form.search-form', modal.body).attr('action');

    /* save initial page browser HTML, so that we can restore it if the search box gets cleared */
    var initialPageResultsHtml = $('.page-results', modal.body).html();

    var request;

    function search() {
      var query = $('#id_q', modal.body).val();
      if (query != '') {
        request = $.ajax({
          url: searchUrl,
          data: {
            q: query,
            results_only: true
          },
          success: function(data, status) {
            request = null;
            $('.page-results', modal.body).html(data);
            ajaxifySearchResults();
          },
          error: function() {
            request = null;
          }
        });
      } else {
        /* search box is empty - restore original page browser HTML */
        $('.page-results', modal.body).html(initialPageResultsHtml);
        ajaxifyBrowseResults();
      }
      return false;
    }

    $('#id_q', modal.body).on('input', function() {
      if (request) {
        request.abort();
      }
      clearTimeout($.data(this, 'timer'));
      var wait = setTimeout(search, 200);
      $(this).data('timer', wait);
    });

    /* Set up behaviour of choose-page links in the newly-loaded search results,
      to pass control back to the calling page */
    function ajaxifySearchResults() {
      $('.page-results a.choose-page', modal.body).on('click', function() {
        var pageData = $(this).data();
        modal.respond('pageChosen', $(this).data());
        modal.close();

        return false;
      });
      /* pagination links within search results should be AJAX-fetched
          and the result loaded into .page-results (and ajaxified) */
      $('.page-results a.navigate-pages', modal.body).on('click', function() {
        $('.page-results', modal.body).load(this.href, ajaxifySearchResults);
        return false;
      });
      /* Set up parent navigation links (.navigate-parent) to open in the modal */
      $('.page-results a.navigate-parent', modal.body).on('click', function() {
        modal.loadUrl(this.href);
        return false;
      });
    }

    function ajaxifyBrowseResults() {
      /* Set up page navigation links to open in the modal */
      $('.page-results a.navigate-pages', modal.body).on('click', function() {
        modal.loadUrl(this.href);
        return false;
      });

      /* Set up behaviour of choose-page links, to pass control back to the calling page */
      $('a.choose-page', modal.body).on('click', function() {
        var pageData = $(this).data();
        pageData.parentId = jsonData['parent_page_id'];
        modal.respond('pageChosen', $(this).data());
        modal.close();

        return false;
      });
    }
    ajaxifyBrowseResults();

    /*
      Focus on the search box when opening the modal.
      FIXME: this doesn't seem to have any effect (at least on Chrome)
      */
    $('#id_q', modal.body).trigger('focus');
  },

  anchor_link: function(modal, jsonData) {
    $('p.link-types a', modal.body).on('click', function() {
      modal.loadUrl(this.href);
      return false;
    });

    $('form', modal.body).on('submit', function() {
      modal.postForm(this.action, $(this).serialize());
      return false;
    });
  },
  email_link: function(modal, jsonData) {
    $('p.link-types a', modal.body).on('click', function() {
      modal.loadUrl(this.href);
      return false;
    });

    $('form', modal.body).on('submit', function() {
      modal.postForm(this.action, $(this).serialize());
      return false;
    });
  },
  phone_link: function(modal, jsonData) {
    $('p.link-types a', modal.body).on('click', function() {
      modal.loadUrl(this.href);
      return false;
    });

    $('form', modal.body).on('submit', function() {
      modal.postForm(this.action, $(this).serialize());
      return false;
    });
  },
  external_link: function(modal, jsonData) {
    $('p.link-types a', modal.body).on('click', function() {
      modal.loadUrl(this.href);
      return false;
    });

    $('form', modal.body).on('submit', function() {
      modal.postForm(this.action, $(this).serialize());
      return false;
    });
  },
  external_link_chosen: function(modal, jsonData) {
    modal.respond('pageChosen', jsonData['result']);
    modal.close();
  }
};

(function() {
  'use strict';

  function createLink(pageData, currentText) {
    var a, text;

    // Create link
    a = document.createElement('a');
    a.setAttribute('href', pageData.url);
    if (pageData.id) {
      a.setAttribute('data-id', pageData.id);
      a.setAttribute('data-parent-id', pageData.parentId);
      a.setAttribute('data-linktype', 'page');
      // If it's a link to an internal page, `pageData.title` will not use the link_text
      // like external and email responses do, overwriting selection text :(
      text = currentText || pageData.title;
    } else {
      text = pageData.title;
    }
    a.appendChild(document.createTextNode(text));

    return a;
  }

  (function($) {
    tinymce.PluginManager.add('wagtaillink', function(editor) {
      function showDialog() {
        var url,
          urlParams,
          mceSelection,
          $currentNode,
          $targetNode,
          currentText,
          insertElement;

        currentText = '';
        url = window.chooserUrls.pageChooser;
        urlParams = {
          allow_external_link: true,
          allow_email_link: true
        };

        mceSelection = editor.selection;
        $currentNode = $(mceSelection.getEnd());
        // target selected link (if any)
        $targetNode = $currentNode.closest('a[href]');

        if ($targetNode.length) {
          currentText = $targetNode.text();
          var linkType = $targetNode.data('linktype');
          var parentPageId = $targetNode.data('parent-id');
          var href = $targetNode.attr('href');
          if (linkType == 'page' && parentPageId) {
            url =
              window.chooserUrls.pageChooser + parentPageId.toString() + '/';
          } else if (href.startsWith('mailto:')) {
            url = window.chooserUrls.emailLinkChooser;
            href = href.replace('mailto:', '');
            urlParams['link_url'] = href;
          } else if (!linkType) {
            url = window.chooserUrls.externalLinkChooser;
            urlParams['link_url'] = href;
          }
          if ($targetNode.children().length == 0) {
            // select and replace text-only target
            insertElement = function(elem) {
              mceSelection.select($targetNode.get(0));
              mceSelection.setNode(elem);
            };
          } else {
            // replace attributes of complex target
            insertElement = function(elem) {
              mceSelection.select($targetNode.get(0));
              var $elem = $(elem);
              $targetNode.attr('href', $elem.attr('href'));
              if ($elem.data('linktype')) {
                $targetNode.data($elem.data());
              } else {
                $targetNode.removeData('linktype');
                $targetNode.removeAttr('data-linktype');
              }
            };
          }
        } else {
          if (!mceSelection.isCollapsed()) {
            currentText = mceSelection.getContent({ format: 'text' });
          }
          // replace current selection
          insertElement = function(elem) {
            mceSelection.setNode(elem);
          };
        }

        urlParams['link_text'] = currentText;

        ModalWorkflow({
          url: url,
          urlParams: urlParams,
          onload: TINYMCE_PAGE_CHOOSER_MODAL_ONLOAD_HANDLERS,
          responses: {
            pageChosen: function(pageData) {
              editor.undoManager.transact(function() {
                editor.focus();
                insertElement(createLink(pageData, currentText));
              });
            }
          }
        });
      }

      editor.addButton('link', {
        icon: 'link',
        tooltip: 'Insert/edit link',
        shortcut: 'Meta+K',
        onclick: showDialog,
        stateSelector: 'a[data-linktype=page],a[href]:not([data-linktype])'
      });

      editor.addButton('unlink', {
        icon: 'unlink',
        tooltip: 'Remove link',
        cmd: 'unlink',
        stateSelector: 'a[data-linktype=page],a[href]'
      });

      editor.addMenuItem('link', {
        icon: 'link',
        text: 'Insert/edit link',
        shortcut: 'Meta+K',
        onclick: showDialog,
        stateSelector: 'a[data-linktype=page],a[href]:not([data-linktype])',
        context: 'insert',
        prependToContext: true
      });

      editor.addShortcut('Meta+K', '', showDialog);
      editor.addCommand('mceLink', showDialog);
    });
  })(jQuery);
}.call(this));
